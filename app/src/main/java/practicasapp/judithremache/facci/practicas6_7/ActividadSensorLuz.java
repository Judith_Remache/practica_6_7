package practicasapp.judithremache.facci.practicas6_7;

import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import static android.hardware.Sensor.TYPE_LIGHT;

public class ActividadSensorLuz extends AppCompatActivity {

    private SensorManager sensorManager;
    private Sensor linghtSensor;
    private SensorEventListener lightEventListener;
    private View root;
    private float maxValue;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_sensor_luz);

        root=findViewById(R.id.root);
        sensorManager=(SensorManager)getSystemService(SENSOR_SERVICE);
        linghtSensor= sensorManager.getDefaultSensor(TYPE_LIGHT);

        if (linghtSensor==null){
            Toast.makeText(this,"El dispositivo no tiene sensor de longitud:",Toast.LENGTH_SHORT).show();
           finish();
        }
        maxValue = linghtSensor.getMaximumRange();

        lightEventListener= new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent sensorEvent) {
                float value = sensorEvent.values[0];
                getSupportActionBar().setTitle("Luminosidad:" + value +"lx");
                int newValue =(int) (255f * value/ maxValue);
                root.setBackgroundColor(Color.rgb(newValue,newValue,newValue));
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {

            }
        };
    }


    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(lightEventListener,linghtSensor,SensorManager.SENSOR_DELAY_FASTEST);
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(lightEventListener);
    }
}
