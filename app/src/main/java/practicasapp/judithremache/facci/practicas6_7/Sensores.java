package practicasapp.judithremache.facci.practicas6_7;

import android.content.Context;
import android.content.Intent;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class Sensores extends AppCompatActivity {

    Button BAcelerometro,BProximinidad,BLuminosidad,BVibrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensores);

        BAcelerometro =(Button)findViewById(R.id.btnAcelerometro);
        BProximinidad = (Button)findViewById(R.id.btnProximidad);
        BLuminosidad = (Button)findViewById(R.id.btnLuminosidad);
        BVibrar =(Button)findViewById(R.id.btnVibrar);

        final Vibrator vibrator =(Vibrator)this.getSystemService(Context.VIBRATOR_SERVICE);

        BVibrar = (Button)findViewById(R.id.btnVibrar);
        BVibrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vibrator.vibrate(600);
        }
        });

        BAcelerometro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(
                        Sensores.this, ActividadSensorAcelerometro.class);
                startActivity(intent);
            }
        });

        BProximinidad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(
                        Sensores.this, ActividadSensorProximidad.class);
                startActivity(intent);
            }
        });
        BLuminosidad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(
                        Sensores.this, ActividadSensorLuz.class);
                startActivity(intent);
            }
        });
    }


}
